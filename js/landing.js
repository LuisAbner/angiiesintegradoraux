window.addEventListener('DOMContentLoaded', event => {

    // Navbar shrink function
    var navbarShrink = function() {
        const navbarCollapsible = document.body.querySelector('#mainNav');
        if (!navbarCollapsible) {
            return;
        }
        if (window.scrollY === 0) {
            navbarCollapsible.classList.remove('navbar-shrink')
        } else {
            navbarCollapsible.classList.add('navbar-shrink')
        }

    };

    // Shrink the navbar 
    navbarShrink();

    // Shrink the navbar when page is scrolled
    document.addEventListener('scroll', navbarShrink);

    // Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav) {
        new bootstrap.ScrollSpy(document.body, {
            target: '#mainNav',
            offset: 74,
        });
    };

    // Collapse responsive navbar when toggler is visible
    const navbarToggler = document.body.querySelector('.navbar-toggler');
    const responsiveNavItems = [].slice.call(
        document.querySelectorAll('#navbarResponsive .nav-link')
    );
    responsiveNavItems.map(function(responsiveNavItem) {
        responsiveNavItem.addEventListener('click', () => {
            if (window.getComputedStyle(navbarToggler).display !== 'none') {
                navbarToggler.click();
            }
        });
    });


});



var myApp = angular.module('Angies', []);

myApp.controller('landing', ['$scope', function($scope) {
    $scope.variablePrueba = "Prueba"
    $scope.validaciones = {
        nombre: "Marco Antonio",
        password: "123456",
        email: "alviter@gmail.com",
        apellido: "Alviter Rodriguez",
        sexo: "1",
        fechaNacimiento: "",
        calle: "Barrio de la ventana ",
        numeroCalle: 16,
        pais: "Maxico",
        estado: "Morelos",

    }

    $scope.summitFromRegistro = () => {
        if ($scope.validaciones.nombre === "" || $scope.validaciones.password === "" || $scope.validaciones.email === "" || $scope.validaciones.apellido === "" || $scope.validaciones.sexo === "" || $scope.validaciones.fechaNacimiento === "") {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Ingrese todos los datos requeridos',
            })
        } else {
            Swal.fire({
                icon: 'success',
                title: 'Se ha actualizado tu perfil',
            })
        }

    }
    $scope.carritoDecompras = () => {
        $(location).attr('href', "../bagComplete.html");
    }
    $scope.detallesProducto = () => {
        $(location).attr('href', "DestallesProducto.html");
    }

    $scope.anadirCarrito = () => {
        Swal.fire({
            icon: 'success',
            title: 'Se ha añadido al carrrito de compras',
        })
    }
    $scope.pageError = () => {
        $(location).attr('href', "Error500.html");
    }
    $scope.pageError2 = () => {
        $(location).attr('href', "Error502.html");
    }

}]);