var productName=0;
var description=0;
var supplier=0;
var productType=0;
var stock=0;

(function mostrar(){
    
    $('input[name=option]').on('change', function(){
        var optionUrl = $('input[name="option"]:checked').val();
        console.log("Info:>"+optionUrl);
        
        opc = document.getElementById('optionYes').checked;
        console.log("cambio"+opc);
        if(opc){
            $('#labelUrl').show();
            $('#url').show();
        }else{
            $('#labelUrl').hide();
            $('#url').hide();
        }
    })

    $('#productName').on('change', function(){
        window.productName = document.getElementById('productName').value;
        console.log("valorProductName:>"+productName)
    })
    $('#description').on('change', function(){
        window.description = document.getElementById('description').value;
        console.log("valorProductName:>"+description)
    })
    $('#supplier').on('change', function(){
        window.supplier = document.getElementById('supplier').value;
        console.log("valorProductName:>"+supplier)
    })
    $('#productType').on('change', function(){
        window.productType = document.getElementById('productType').value;
        console.log("valorProductName:>"+productType)
    })
    
    $('#stock').on('change', function(){
        window.stock = document.getElementById('stock').value;    
        console.log("valorProductName:>"+stock.length)
    })

    console.log(productName)
})()


//Validaciones de campos
function validarName(){
    
    if(productName.length>=4){
        return true;
    }else{
        return false;
    }
}

function validarSupplier(){
    if(supplier.length>=5){
        return true;
    }else{
        return false;
    }
}

function validarDescription(){
    if(description.length>=10){
        return true;
    }else{
        return false;
    }
}

function validarProductType(){
    if(productType.length>=4){
        return true;
    }else{
        return false;
    }
}

function validarStock(){
    if(stock>0){
        return true;
    }else{
        return false;
    }
}

$(document).ready(function(){
    console.log("listo")
    
    $('#card1').hide();
    $('#addPhoto').hide();
    $('#btnAdd').on('click',function(){
        $('#addPhoto').click();
    })

    $('#btnGuardar').on('click',function(){
        var nam=validarName()
        var des=validarDescription()
        var proT=validarProductType()
        var sup = validarSupplier()
        var stock = validarStock()
        console.log(nam,des,sup,proT,stock)
       if(nam== true && des==true && sup==true && proT==true && stock==true){
        swal({
            title: 'Registro exitoso!',
            text: 'Se ha registrado correctamente el producto',
            icon: 'success',
            confirmButtonText: 'OK'
            })
            document.getElementById('productName').value="";
            document.getElementById('description').value="";
            document.getElementById('supplier').value="";
            document.getElementById('productType').value="";
            document.getElementById('stock').value="";
            productName=0;
            description=0;
            supplier=0;
            productType=0;
            stock=0;


       }else{
            if(des==false&&sup==true){
                swal({
                    title: 'Descripción muy corta!',
                    text: 'Debes ingresar una descripción más grande',
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }else if(stock==false &&proT==true){
                swal({
                    title: 'Error de Stock!',
                    text: 'Debe haber una existencia al menos en stock',
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }else{
                swal({
                    title: 'Llena todos los campos!',
                    text: 'Debes ingresar todos los campos',
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }
        
       }
       //console.log(productName);
    })
    
})


