var name=0;
var surname=0;
var email=0;
var phone=0;
var coment=0;
var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
var phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
(function obtenerDatos(){

    $('#name').on('change', function(){
        window.name = document.getElementById('name').value;
        console.log("valorProductName:>"+name)
    })
    $('#surname').on('change', function(){
        window.surname = document.getElementById('surname').value;
        console.log("valorProductName:>"+surname)
    })
    $('#email').on('change', function(){
        window.email = document.getElementById('email').value;
        console.log("valorProductName:>"+email)
    })
    $('#tel').on('change', function(){
        window.phone = document.getElementById('tel').value;
        console.log("valorProductName:>"+phone)
    })
    
    $('#coment').on('change', function(){
        window.coment = document.getElementById('coment').value;    
        console.log("valorProductName:>"+coment)
    })

    
})()

//Validaciones de campos
function validarName(){
    if(name.length>=2){
        return true;
    }else{
        return false;
    }
}

function validarSurname(){
    if(surname.length>=2){
        return true;
    }else{
        return false;
    }
}

function validarEmail(){
    if(emailRegex.test(email)){
        return true;
    }else{
        return false;
    }
}

function validarPhone(){
    if(phoneRegex.test(phone)){
        return true;
    }else{
        return false;
    }
}

function validarComent(){
    if(coment.length>=10){
        return true;
    }else{
        return false;
    }
}


$(document).ready(function(){
    console.log("listooo")

    $('#btnEnviar').on('click', function(){
        var vName = validarName();
        var vSurname = validarSurname();
        var vEmail = validarEmail();
        var vPhone = validarPhone();
        var vComent = validarComent();
        
        console.log(vName,vSurname,vEmail,vPhone,vComent)

        if(vName == true && vSurname==true && vEmail==true && vPhone==true && vComent==true){
            swal({
                title: 'Envio Exitoso!',
                text: 'Se envió correctamente tu comentario, nos pondremos en contacto contigo lo antes posible',
                icon: 'success',
                confirmButtonText: 'OK'
                })
                document.getElementById('name').value="";
                document.getElementById('surname').value="";
                document.getElementById('email').value="";
                document.getElementById('tel').value="";
                document.getElementById('coment').value="";
                name=0;
                surname=0;
                email=0;
                phone=0;
                coment=0;
        }else{
            if(vComent==false&&vPhone==true){
                swal({
                    title: 'Comentario muy corto!',
                    text: 'Debes ingresar un comentario más grande',
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }else if(vEmail==false &&vSurname==true && vComent==true){
                swal({
                    title: 'Error de Correo!',
                    text: 'Debes ingresar un correo valido',
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }else if(vPhone==false && vSurname==true && vEmail==true && vComent==true){
                swal({
                    title: 'Error de Teléfono!',
                    text: 'Debes ingresar un teléfono valido',
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }else{
                swal({
                    title: 'Llena todos los campos!',
                    text: 'Debes ingresar todos los campos',
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }
        }
    })
})
